1. -> https://leetcode.com/problems/sqrtx/

// Solution of the above(1) problem...

class Solution {
public:
    int mySqrt(int x) {
        if(x==1 || x==2 || x==3)
            return 1;
        unsigned int l,l2=x;
        for(unsigned int i=1;i<=l2;i++)
        {
            l = i*i;
            if(l == l2)
                return i;
            else if(l > l2)
                return (i-1);
        }
        return x;
    }
};


2. -> https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/

// Solution of the above(2) problem...

// (Take the reference of " https://www.geeksforgeeks.org/find-index-first-1-sorted-array-0s-1s/ " article to solve (2) problem in a efficient way...) 

class Solution {
public:
    int binarySearch(vector<int>& arr, int start, int end, int target)
{
    if (end >= start) 
    {
        int mid = start + (end - start) / 2;

        if (arr[mid] == target)
            return mid;

        else if (arr[mid] > target)
            return binarySearch(arr, start, mid - 1, target);

        else
            return binarySearch(arr, mid + 1, end, target);

    }
        
    return -1;
}
    
    vector<int> searchRange(vector<int>& nums, int target) {
        
        vector<int> out(2, -1);
        
        int start = 0, end = nums.size() - 1, mid;
        //cout<<end;
        
        mid = binarySearch(nums, start, end, target);
        cout<<mid;
        if(mid == -1)
            return out;
        
        else if(mid == 0)
        {
            out.pop_back();
            out.pop_back();
            out.push_back(0);
            for(int i=0;i<=end;i++)
            {
                if(i == end && nums[i] == target)
                {
                    out.push_back(i);
                    break;
                }
                else if(nums[i] != target)
                {
                    out.push_back(i-1);
                    break;
                }
            }
            return out;
        }
        else
        {
            for(int i=mid;i>=0;i--)
            {
                 if(nums[i] != target)
                 {
                     out.pop_back();
                     out.pop_back();
                     out.push_back(i+1);
                     break;
                 }
                if(nums[i] == target && i==0)
                {
                    out.pop_back();
                    out.pop_back();
                    out.push_back(i);
                    break;
                }
            }
            for(int i=mid;i<=end;i++)
            {
                if(i == end && nums[i] == target)
                {
                    out.push_back(i);
                    break;
                }
                else if(nums[i] != target)
                 {
                     out.push_back(i-1);
                     break;
                 }
            }
        }
        return out;

    }
};

