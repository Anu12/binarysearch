Binary search
1.sqrt(x)

class Solution {
    public int mySqrt(int x) {
        if(x==0)
        return 0;
        int start=1, end=x;
        int ans=0;
        while(start<=end)
        {
        int mid = (start + end) / 2;
 
        
            if (mid*mid == x)
                return mid;
 
    
            if (mid*mid < x)
            {
                start = mid + 1;
                ans = mid;
            }
        }
        return ans;
    }
}

2. First and last position
class Solution {
            public static int first(int arr[], int start, int end, int x, int n)
    {
        if (start<=end) {
            int mid = start + (end - start) / 2;
            if ((mid == 0 || x > arr[mid - 1]) && arr[mid] == x)
                return mid;
            else if (x > arr[mid])
                return first(arr, (mid + 1), end, x, n);
            else
                return first(arr, start, (mid - 1), x, n);
        }
        return -1;
    }
 
    public static int last(int arr[], int start, int end, int x, int n)
    {
        if (start<=end) {
            int mid = start + (end - start) / 2;
            if ((mid == n - 1 || x < arr[mid + 1]) && arr[mid] == x)
                return mid;
            else if (x < arr[mid])
                return  last(arr, start, (mid - 1), x, n);
            else
                return last(arr, (mid + 1), end, x, n);
        }
        return -1;
    }
        
}
3. Floor and Ceil value
class Solution {
   
        int floor = -1;
        int ceil = -1;
static void binarySearchUtilForFloorAndCeil(int a[], int x, int start, int end) {
    while(start <= end) {
       int mid = start + ((end-start)/2);
       if(x == a[mid]) {floor = mid; ceil = mid; return;}
       else if(x < a[mid]) {
       	  ceil = mid;
          end = mid-1;
       } else {
       	  floor = mid;
          start = mid+1;
       }
    }
    return;
}
}
