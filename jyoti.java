https://leetcode.com/problems/sqrtx/
class Solution {
    public int mySqrt(int x) {
         long left=0;
        long right=x;
        long ans=0;
        while(left<=right){
            
            long mid= left+(right-left)/2;
            if(mid*mid==x){
            ans=mid;
            return (int)ans;
            }
            if(mid*mid <x){
                left=mid+1;
                ans =mid;
            }
            else{
                right= mid-1;
            }
        }
        return (int)ans;
        
    }
}