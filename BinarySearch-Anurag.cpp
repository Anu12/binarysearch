#include<iostream>
using namespace std;


//BINARY SEARCH CODE 

int binarySearchUtil(int a[], int x, int start, int end) {
    while(start <= end) {
       //mid = ((start+end)/2);
       int mid = start + ((end-start)/2);

       if(x == a[mid]) return mid;
       else if(x < a[mid]) {
          end = mid-1;
       } else {
          start = mid+1;
       }
    }
    return -1;
}

int binarySearch(int a[], int n, int x) {
	int start = 0;
	int end = n-1;
    return binarySearchUtil(a,x,start,end);
}

//TimeComplexity -  log(n)
//SpaceComplexity - constant or O(1)


int floor = -1;
int ceil = -1;
void binarySearchUtilForFloorAndCeil(int a[], int x, int start, int end) {
    while(start <= end) {
       int mid = start + ((end-start)/2);
       if(x == a[mid]) {floor = mid; ceil = mid; return;}
       else if(x < a[mid]) {
       	  ceil = mid;
          end = mid-1;
       } else {
       	  floor = mid;
          start = mid+1;
       }
    }
    return;
}