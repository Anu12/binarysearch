Question Link -> https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/

Rough Work and Concept Building 


[0,1,2,4,5,6,7] 

[7,0,1,2,4,5,6] = Input Array , find min element ?

[6,7,0,1,2,4,5]


[1,2,3,4,5]

[5,1,2,3,4]

[2  3  4  5  1]


ans = a[0] ;  array is non-empty and array is non-rotated
ans = a[n] ;  array is rotated n times (0 to n-1)



       [5,1,2,3,4]
    

       min using mid

       if(a[i] < a[i-1] && a[i] < a[i+1])  ; a[i-1] >=0 && a[i+1] < n 
           min_ele = a[i];

       else if(a[i] < a[i-1]) ; a[i-1] >=0 && (i+1)==n
           min_ele = a[i];

       else if(a[i] < a[i+1]) ; a[i+1] <n && (i-1)<0
           min_ele = a[i];

       else  min_ele = a[i]; (i-1)<0 && (i+1)==n


       if(a[mid] < a[0]) {   //first-half is unsorted 
          last = mid-1;
       } else if(a[mid] > a[n-1]) { //second-half is unsorted
           first = mid+1;
       } else  {       //when array is sorted
          last = mid-1;
       }
       
       
       
### Complete Working Recursive Code ###

#vector<int> nums ---> similar to an array
nums.size() ---> n

int &ans --> ans variable passed by Reference

class Solution {
public:
    void fn(vector<int>& nums, int beg, int end, int &ans);
    int findMin(vector<int>& nums) {
        int ans = -1;
        fn(nums,0,nums.size()-1,ans);
        return ans;
    }
};

void Solution::fn(vector<int>& nums, int beg, int end, int &ans) {
    if(beg>end) return;
    int mid = (beg + ((end-beg)/2));
    
    if(mid-1 >= 0 && mid+1 < nums.size()) {
        if(nums[mid] < nums[mid-1] && nums[mid] < nums[mid+1]) {
            ans = nums[mid];
        }
    } else if(mid-1 >= 0) {
        if(nums[mid] < nums[mid-1]) {
            ans = nums[mid];
        }
    } else if(mid+1 < nums.size()) {
        if(nums[mid] < nums[mid+1]) {
            ans = nums[mid];
        }
    } else if(mid-1 < 0 && mid+1 >= nums.size()) {
        ans = nums[mid];
    }
    
    if(ans != -1) return;
    
    //Look for Unsorted Array
    if(nums[beg] > nums[mid]) fn(nums,beg,mid-1,ans);
    else if(nums[end] < nums[mid]) fn(nums,mid+1,end,ans);
    //If both ends are sorted , now go into left end
    else if(nums[beg] < nums[mid]) fn(nums,beg,mid-1,ans);
}


