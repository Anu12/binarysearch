#include <iostream>
#include <stack> 
#include <queue>
#include <map> 
#include <set> 
#include<unordered_map>
#include<unordered_set>
/*
//stack ,queue , map , set 

STACK - LIFO , FILO 
Last In First Out
First In Last Out

4
3
2
1

Queue - First In First Out FIFO 

Out ----------------- In 
                       
                           1  2  3   4   5  6 
                           
sets and maps ---> SORTED , Balanced Binary Search Tree (Non-Linear Implementation)


unordered sets and unordered maps ---> UNSORTED (Hash Buckets)

map ---> Mapping 
1   3   4   2   1    2   5  4 
key - value pair   unorder_map
1       2 
3       1 
4       2 
2       2 
5       1 

map  
key   value 
1    2    
2    2
3    1
4    2
5    1

                           
*/                           
using namespace std;

int main()
{
    stack<int> s1;
    queue<int> q1;
    
    //Stack  In  --> Push()
    //Stack out --> Pop()
    s1.push(1);
    s1.push(5);
    s1.push(21);
    s1.pop();
    cout<<s1.top()<<endl;
    cout<<s1.top()<<endl;
    cout<<s1.empty()<<endl;
    
    //Queue In --> push()
    //Queue Out --> Pop()
    q1.push(2);
    q1.push(8);
    cout<<q1.front()<<endl;
    cout<<q1.back()<<endl;
    q1.pop();
    cout<<q1.front()<<endl;
    cout<<q1.back()<<endl;
    cout<<q1.empty()<<endl<<endl;
    
    cout<<"set and map"<<endl;
    
    set<int> se1;
    unordered_set<int> use1;
    map<int,int> m1;
    unordered_map<int,int> um1;
    
    set<int> :: iterator sit;
    unordered_set<int> :: iterator usit;
    map<int,int> :: iterator mit;
    unordered_map<int,int> :: iterator umit;
    
    se1.insert(10);
    se1.insert(1);
    se1.insert(-34);
    
    for(sit=se1.begin();sit!=se1.end();sit++) {
        cout<<*sit<<endl;
    }
    
    
    cout<<endl<<endl;
    
    use1.insert(0);
    use1.insert(2);
    use1.insert(-334);
    
    for(usit=use1.begin();usit!=use1.end();usit++) {
        cout<<*usit<<endl;
    }
    
    
    m1.insert(make_pair(2,3));
    m1.insert({1,1});
    m1.insert({1,2});  //it will not be overwritten this way
    //m1[1]=2;  //how to overwrite value at same key 
    for(mit=m1.begin();mit != m1.end();mit++) {
        cout<<mit->first<<" "<<mit->second<<endl;
    }
    //https://www.geeksforgeeks.org/map-associative-containers-the-c-standard-template-library-stl/


    return 0;
}