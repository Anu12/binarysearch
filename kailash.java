1. sqrt
class Solution {
    public int mySqrt(int x) {
         if(x==1 || x==2 || x==3)
            return 1;
        int k,p=x;
        for(int i=1;i<=p;i++)
        {
            k = i*i;
            if(k == p)
                return i;
            else if(k > p)
                return (i-1);
        }
        return x;
    }
}
2./find-first-and-last-position-of-element-in-sorted-array
class Solution {
   public int[] searchRange(int[] nums, int target) {
        
        int[] result = new int[2];
        result[0] = first(nums,target);
        result[1] = last(nums,target);
        return result;
        
    }
    
    public int first(int[] nums, int target){
        
        int result = -1;
        int low = 0;
        int high = nums.length - 1;

        while(low <= high){
            int mid = low + ((high-low)/2);

            if (nums[mid] < target){
                low = mid +1;
            } else if (nums[mid] > target){
                high = mid - 1;
            } else {
                result = mid;

              
                high = mid - 1; 
   
            }
        }

        return result;
  
    }
    
        public int last(int[] nums, int target){
        
        int result = -1;
        int low = 0;
        int high = nums.length - 1;
        
        while(low <= high){
            
            int mid = low + (high-low)/2;
            
            if (nums[mid] < target){
                low = mid +1;
            } else if (nums[mid] > target){
                high = mid - 1;
            } else { // nums[mid] == target
                result = mid;
              
                low = mid + 1;
   
            }
        }

        return result;
    }
}