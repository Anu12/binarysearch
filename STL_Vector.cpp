/***
Documentation Link -> https://www.topcoder.com/thrive/articles/Power%20up%20C++%20with%20the%20Standard%20Template%20Library%20Part%20One
***/

#include<vector>
#include<iostream>
using namespace std;

int main()
{
    /*
    cout<<"Hello World";
    Stl -> C++
    Collections -> Java
    1000000000000
    CONTAINERS
    
    vector --> arraylist
    pair ->
    stack/queue  -> stack/queue
    sets and maps  -> hashtables , hashmaps , treemaps
    */
    
    //container_type<datatype> container_name; 
    vector<int> v;
    cout<<v.size()<<endl;         //0
    cout<<v.capacity()<<endl;    //0
    //push_back
    v.push_back(1);
     cout<<"first time ="<< v.size()<<endl; 
    v.push_back(10);
    cout<<"second time ="<< v.size()<<endl; 
    v.push_back(100);
    cout<<"third time ="<< v.size()<<endl; 
    v.push_back(1000);
    cout<<"fourth time ="<< v.size()<<endl; 
    cout<<v.size()<<endl;          //1
    cout<<v.capacity()<<endl;
    
    //container_type<datatype> :: iterator iterator_name;
    vector<int> :: iterator vit;    //begin() --> end()
    vector<int> :: reverse_iterator rvit;    //rbegin()  --> rend()
    
    for(rvit = v.rbegin(); rvit != v.rend(); rvit++) {
        cout<<*rvit<<endl;
    }
    
    
//Vector of size 3 having a,v,d  where $ and * are invalid postions in beginning and end resp.

//  $    a     v    d   * 
//    begin()  --> a
//    end() --> *
//    rbegin() --> d
//    rend() --> $
    
    cout<<v.front()<<endl;
    cout<<v.back()<<endl;
    
    /*
    vector<int> v2(4);
    cout<<v2.size()<<endl;
    cout<<v2[0]<<endl;
    cout<<v2[1]<<endl;
    cout<<v2[1000]<<endl;
    cout<<v2.size()<<endl;
    v2.push_back(1000);
    cout<<v2.size()<<endl;
    v2.clear();
    cout<<v2.size()<<endl;
    cout<<v2.empty()<<endl;
    vector<string> str_v;
    str_v.push_back("Newton School ");
    str_v.push_back("Mentor Session goes from ");
    str_v.push_back("11 to 12");
    for(int i=0; i<str_v.size(); i++) {
        cout<<str_v[i];
    }
    //push_back()
    //size()
    //capacity()
    //empty()
    //insert/erase
    
    //(v.empty() -> true/FALSE
    
    //ITERATORS 
    begin()
    end()
    
    
    
    a    v      b   j   ....
    
    */
    
     return 0;
}